const slider = document.querySelector('#slider')
const canvas = document.querySelector('#canvas')


slider.addEventListener('input', CargarRango)


const ctx = canvas.getContext('2d')

const images = []


window.addEventListener('load', pageLoaded)


function pageLoaded() {
  for (let i = 1; i <= 36; i += 1) {
    const num = i.toString().padStart(2, '0')
    const url = `https://stockx-360.imgix.net/Nike-Air-Force-1-Low-Chinese-New-Year-2019/Images/Nike-Air-Force-1-Low-Chinese-New-Year-2019/Lv2/img${num}.jpg?auto=format,compress&q=90&updated_at=1561408941&w=1000`
    const image = new Image()
    image.src = url
    image.addEventListener('load', () => {
      images[i] = image
      if (i === 1) {
        CargarImagenes(i)
      }
    })
  }
}
function CargarImagenes(index) {
  ctx.drawImage(images[index], 0, 0, canvas.width, canvas.height)
}



function CargarRango() {
  CargarImagenes(this.value)
}